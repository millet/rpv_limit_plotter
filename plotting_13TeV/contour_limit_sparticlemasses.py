#!/usr/bin/env python
from __future__ import division
import sys
import logging as log
import numpy as np
import ROOT as r
import argparse
from collections import OrderedDict
from rootpy.io import root_open
from rootpy.plotting import root2matplotlib as rplt
import matplotlib as mpl
import matplotlib.tri as mtri
from matplotlib import rc
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import griddata,interpolate
import scipy


class axis_spec():
    def __init__(self, ax_min, ax_max, ax_label = ""):
        self.limits = [ax_min,ax_max]
        self.ax_min = ax_min
        self.ax_max = ax_max
        self.label = ax_label

class format_clabel(float):
     def __repr__(self):
         str = '%.3f' % (self.__float__(),)
         if str[-1]=='0':
             return '%.2f' % self.__float__()
         else:
             return '%.3f' % self.__float__()

class format_clabelMH(float):
     def __repr__(self):
	 return r'$m_{\mathrm{H}}$'+' = {0:.0f} GeV'.format(self.__float__())

def read_xs(m0,m12):
    xs_file = open("xsec_nlo_muon.txt", 'r')
    for line in xs_file:
        line_split = line.split()
        #xs is in 3rd column of file
        if len(line_split) > 3:
            if (float(line_split[0])==m0) and (float(line_split[1])==m12):
                return float(line_split[2])
        else:
            log.error("Error while parsing the following line of the config file (not enough coulumns.",line_split)
            sys.exit()
    log.debug("Could not read xs for the following point (m0=%f,m12=%f)"%(m0,m12))
    return 0.

def read_line_from_file(file, columns = [0,1]):
    try:
        f = open(file, 'rU')
    except:
        log.error("Could not read file:%s"%(file))
        sys.exit()
    content = OrderedDict()
    for i in range(0,len(columns)):
        col = columns[i]
        content[str(col)] = []
    for line in f:
        line = line.split()
        if line[0][0] == '#':
            continue
        for i in range(0,len(columns)):
            col = columns[i]
            content[str(col)].append(float(line[col]))
    f.close()
    return tuple(content.values())

def long_edges(x, y, triangles, radio=120):
    out = []
    for points in triangles:
        #print points
        a,b,c = points
        d0 = np.sqrt( (x[a] - x[b]) **2 + (y[a] - y[b])**2 )
        d1 = np.sqrt( (x[b] - x[c]) **2 + (y[b] - y[c])**2 )
        d2 = np.sqrt( (x[c] - x[a]) **2 + (y[c] - y[a])**2 )
        max_edge = max([d0, d1, d2])
        #print points, max_edge
        if max_edge > radio:
            out.append(True)
        else:
            out.append(False)
    return out


def higgsMassPlot(value, args):
    x, y, z = read_line_from_file(args.mHFile, columns = [0, 1, 2])
    print min(z),max(z)
    xi, yi = np.meshgrid(np.arange(0,3050,50),np.arange(0,3200,50))
    triang = mtri.Triangulation(x, y)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)
    dx = (xi[2][2]-xi[1][1])
    xi_n = xi - dx/2.
    dy = (yi[2][2]-yi[1][1])
    yi_n = yi - dy/2.
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))
    img = plt.pcolor(xi_n,yi_n,zi, vmin = 100., vmax=130., norm=mpl.colors.LogNorm())
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(img, cax=cax)
    cbar.set_label('higgsmasses',fontsize=45)
    c = ax.contour(xi, yi, zi, levels = [110,120,124,125,126], colors='black', extrapolate = True, interp = True)
    c.levels = [format_clabelMH(val) for val in c.levels ]
    fmt = r'%r'
    ax.clabel(c, fontsize=18, fmt = fmt)
    plt.show()
    c.levels = [format_clabel(val) for val in c.levels ]
    xList, yList = [], []
    for path in c.collections[0].get_paths():
        v = path.vertices
        xList.append(v[:,0])
        yList.append(v[:,1])
    return xList,yList

def getContourLine(value, column, args):
    x, y, z = read_line_from_file(args.inputFile, columns = [0, 1, column])
    xi, yi = np.meshgrid(np.arange(0,3050,50),np.arange(0,3200,50))
    triang = mtri.Triangulation(x, y)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)
    c = plt.contour(xi, yi, zi, levels = [value], colors='black', extrapolate = True, interp = True)
    c.levels = [format_clabel(val) for val in c.levels ]
    xList, yList = [], []
    for path in c.collections[0].get_paths():
        v = path.vertices
        xList.append(v[:,0])
        yList.append(v[:,1])
    return xList,yList

def drawmHContourLine(ax, value, args):
    x, y, z = read_line_from_file(args.mHFile, columns = [0, 1, 2])
    if value == 126:
        xi, yi = np.meshgrid(np.arange(1000,1750,1),np.arange(2900,3200,1))
    elif value == 125:
        xi, yi = np.meshgrid(np.arange(750,2550,1),np.arange(2200,2600,1))
    elif value == 124:
        xi, yi = np.meshgrid(np.arange(600,2700,1),np.arange(1800,2200,1))
    else:
        xi, yi = np.meshgrid(np.arange(0,3050,50),np.arange(0,3200,50))
    triang = mtri.Triangulation(x, y)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)
    c = ax.contour(xi, yi, zi, levels = [value], colors='black', linestyles = 'dashed', extrapolate = True, interp = True)
    c.levels = [format_clabelMH(val) for val in c.levels ]
    fmt = r'%r'
    if value == 126:
        manual_location = [(1750,3000)]
    elif value == 125:
        manual_location = [(2300,3000)]
    elif value == 124:
        manual_location = [(900,3000)]
    else:
        manual_location = [(1300,3000)]
    ax.clabel(c, fontsize=16, fmt=fmt, manual=manual_location, inline=1, inline_spacing = 15)
    # c = plt.contour(xi, yi, zi, levels = [value], colors='black', extrapolate = True, interp = True)
    # c.levels = [format_clabelMH(val) for val in c.levels ]
    # xList, yList = [], []
    # for path in c.collections[0].get_paths():
    #     v = path.vertices
    #     x = v[:,0]
    #     y = v[:,1]
    #     ax.plot(x, y, c='gray', linestyle = '--', linewidth=2)
	# fmt = r'%r'
	# ax.clabel(c, fontsize=18, fmt = fmt)

def drawContourLine(ax, value, column, args):
    x, y, z = read_line_from_file(args.inputFile, columns = [0, 1, column])
    xi, yi = np.meshgrid(np.arange(0,3050,50),np.arange(0,3200,50))
    triang = mtri.Triangulation(x, y)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)
    c = plt.contour(xi, yi, zi, levels = [value], colors='black', extrapolate = True, interp = True)
    c.levels = [format_clabel(val) for val in c.levels ]
    xList, yList = [], []
    for path in c.collections[0].get_paths():
        v = path.vertices
        x = v[:,0]
        y = v[:,1]
        ax.plot(x, y, c='black', linestyle = '--', linewidth=2)

def draw_lp_limit_from_table(column, args):
    x, y, z = read_line_from_file(args.inputFile, columns = [0, 1, column])
    xi, yi = np.meshgrid(np.arange(0,3050,50),np.arange(0,3050,50))


    # set axes range and labels
    x_label = r'$m_{\widetilde{\chi}_{1}^{0}} (GeV)}$'
    y_label = r'$m_{\widetilde{\mu}} (GeV)$'
    z_label = r'$\lambda^{\prime}_{211}$'
    x_spec = axis_spec(175,3025,x_label)
    y_spec = axis_spec(75,1525,y_label)
    z_spec = axis_spec(0.0005,0.1,z_label)

    triang = mtri.Triangulation(x, y)
    #mask = long_edges(x,y, triang.triangles)
    #triang.set_mask(mask)
    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi = interp_cubic_geom(xi, yi)

    font = {'size': 25}
    mpl.rc('font', **font)
    mpl.rcParams['lines.linewidth'] = 2
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))

    cont = [0.001,0.002,0.003,0.004,0.005,0.01]
    interior = None
    draw_interp_subplot(ax, zi, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi, showGrids = args.showGrids, cont = cont, triang = triang, masked_region = interior)

    if column == 7:
        legendLabel = '13 TeV (obs)'
    elif column == 4:
        legendLabel = 'expected limit'
    cms13tev_limit = ax.plot([-1],[-1], c='black', linestyle = '-', label = legendLabel, linewidth=2)
    ax.legend(loc=(0.55,0.94),prop={'size':30},frameon = False)

    plt.tight_layout()
    if args.show:
        plt.show()
    else:
        basename = 'sparticle_limit'
        if column == 7:
            basename += '_obs'
        elif column == 4:
            basename += '_exp'
        else:
            log.error('Unknown colum: {:d}'.format(column))
            sys.exit(1)
        if args.showGrids:
            basename += '_grids'
        if args.tag is not None:
	    basename += '_' + args.tag
        plt.savefig(basename+'.pdf',bbox_inches='tight')
        plt.savefig(basename+'.png',bbox_inches='tight')

def check_neighbours(h,i,j):
    #return true if one of the neighouring bins is empty / false otherwise
    #lists = [(i,j-1),(i+1,j),(i,j+1),(i-1,j)]
    lists = [(i+1,j),(i,j+1)]
    empty = []
    for x,y in lists:
        if i < 1 or j < 1:
            continue
        if h.GetBinContent(x,y) == 0:
            empty.append([x,y])
    return empty


def interpolate_borders(h_limit, x, y, z):
    for i in range(1,h_limit.GetNbinsX()+1):
        for j in range(1,h_limit.GetNbinsY()+1):
            limit_lp = h_limit.GetBinContent(i,j)
            if abs(limit_lp) == 0:
                continue
            empty_bins = check_neighbours(h_limit,i,j)
            if len(empty_bins) > 0:
                m0_ij = h_limit.GetXaxis().GetBinCenter(i)
                m12_ij = h_limit.GetYaxis().GetBinCenter(j)
                if not (m0_ij == 1500 or m12_ij == 2500):
                    continue
                for k,l in empty_bins:
                    m0_kl = h_limit.GetXaxis().GetBinCenter(k)
                    m12_kl = h_limit.GetYaxis().GetBinCenter(l)
                    if i == k:
                        dist = h_limit.GetXaxis().GetBinWidth(i)
                        if j > l:
                            extrap_limit = ((h_limit.GetBinContent(i,j+1)-h_limit.GetBinContent(i,j))/dist)*(dist/2)+limit_lp
                            y.append(m12_ij-dist/2)
                        else:
                            extrap_limit = ((h_limit.GetBinContent(i,j)-h_limit.GetBinContent(i,j-1))/dist)*(dist/2)+limit_lp
                            y.append(m12_ij+dist/2)
                        x.append(m0_ij)
                    elif j == l:
                        dist = h_limit.GetYaxis().GetBinWidth(j)
                        if i > k:
                            extrap_limit = ((h_limit.GetBinContent(i+1,j)-h_limit.GetBinContent(i,j))/dist)*(dist/2)+limit_lp
                            x.append(m0_ij-dist/2)
                        else:
                            extrap_limit = ((h_limit.GetBinContent(i,j)-h_limit.GetBinContent(i-1,j))/dist)*(dist/2)+limit_lp
                            x.append(m0_ij+dist/2)
                        y.append(m12_ij)
                    z.append(extrap_limit)


def mask_regions(hist,xi,yi,zi):
    for (x,y), value in np.ndenumerate(zi):
        x_tmp = xi[x,y]
        y_tmp = yi[x,y]
        i = hist.GetXaxis().FindBin(x_tmp)
        j = hist.GetYaxis().FindBin(y_tmp)
        if hist.GetBinContent(i,j) == 0:
            zi[x,y] = np.ma.masked


def mask_regions_line(hist,xi,yi):
        xi_new, yi_new = [], []
        for x_tmp,y_tmp in zip(xi,yi):
            i = hist.GetXaxis().FindBin(float(x_tmp))
            j = hist.GetYaxis().FindBin(float(y_tmp))
            if not hist.GetBinContent(i,j) == 0:
                xi_new.append(x_tmp)
                yi_new.append(y_tmp)
        return xi_new,yi_new


def draw_interp_subplot(ax, limit, x_spec, y_spec, z_spec, triang = None, xi = None, yi = None, zi = None, cont = None, title = "", showGrids = False, masked_region = None):
    if isinstance(limit,r.TH2):
        img = rplt.imshow(limit, ax, norm=mpl.colors.LogNorm(),  vmin = z_spec.ax_min , vmax = z_spec.ax_max)
    elif isinstance(limit,np.ma.core.MaskedArray):
        dx = (xi[2][2]-xi[1][1])
        xi_n = xi - dx/2.
        dy = (yi[2][2]-yi[1][1])
        yi_n = yi - dy/2.
        img = ax.pcolor(xi_n,yi_n,limit, vmin = z_spec.ax_min, vmax = z_spec.ax_max, norm=mpl.colors.LogNorm())
    else:
        log.error("Argument 'limit' has wrong type: %s"%type(limit))
        sys.exit()
    ax.set_xlim(x_spec.limits)
    ax.set_xlabel(x_spec.label, fontsize = 45)
    ax.set_ylim(y_spec.limits)
    ax.set_ylabel(y_spec.label, fontsize = 45)
    ax.set_title(title)
    if not xi==None and not yi==None and not zi==None and cont == None:
        c = ax.contour(xi, yi, zi, colors='black')
        ax.clabel(c, inline=1, fontsize=15)
    if not xi==None and not yi==None and not zi==None:
        if masked_region == None:
            c = ax.contour(xi, yi, zi, levels = cont, colors='black', extrapolate = True, interp = True)
        else:
            zi[masked_region] = np.ma.masked
            c = ax.contour(xi, yi, zi, levels = cont, colors='black', extrapolate = True, interp = True)
        c.levels = [format_clabel(val) for val in c.levels ]
        # p = c.collections[0].get_paths()[0]
        # v = p.vertices
        # x = v[:,0]
        # y = v[:,1]
        # for i in range(len(x)):
        #     print x[i],y[i]

        fmt = r'%r'
        ax.clabel(c, fontsize=18, fmt = fmt)
    if showGrids:
        if triang:
            ax.triplot(triang, 'ko-')
        if (not xi == None):
            ax.plot(xi_n, yi_n, 'k-', alpha=0.5)
        if (not yi == None):
            ax.plot(xi_n.T, yi_n.T, 'k-', alpha=0.5)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(img, cax=cax)
    cbar.set_label(z_spec.label,fontsize=45)


def check_interpolation(x, y, z, xi, yi, h, x_spec, y_spec, z_spec, showGrids = False):

    triang = mtri.Triangulation(x, y)

    interp_lin = mtri.LinearTriInterpolator(triang, z)
    zi_lin = interp_lin(xi, yi)

    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
    zi_cubic_geom = interp_cubic_geom(xi, yi)

    interp_cubic_min_E = mtri.CubicTriInterpolator(triang, z, kind='min_E')
    zi_cubic_min_E = interp_cubic_min_E(xi, yi)

    mask_regions(h,xi,yi,zi_lin)
    mask_regions(h,xi,yi,zi_cubic_geom)
    mask_regions(h,xi,yi,zi_cubic_min_E)

    # first figure
    font = {'size': 15}
    mpl.rc('font', **font)
    fig1, ((ax11, ax12), (ax13, ax14)) = plt.subplots(nrows=2, ncols=2, figsize=(25, 25))

    cont = [0.01,0.02,0.03]

    # first subplot: triangulation of limit plot and original limit plot
    if showGrids:
        title = 'original limit with triangular grid'
        draw_interp_subplot(ax11, h, x_spec, y_spec, z_spec, triang = triang, title = title, showGrids = showGrids, cont = cont)
    else:
        title = 'original limitplot'
        draw_interp_subplot(ax11, h, x_spec, y_spec, z_spec, title = title, showGrids = showGrids, cont = cont)
    print 'Triangular grid done'

    # second subplot: original limit with contours from linear interpolation and interpolation grid
    if showGrids:
        title = 'original limit with interp. grid and contours (linear interp.)'
    else:
        title = 'original limit with contours (linear interp.)'
    draw_interp_subplot(ax12, h, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_lin, title = title, showGrids = showGrids, cont = cont)
    print 'Linear interpolation done'

    ## third subplot: original limit with contours from cubic-geom. interpolation and interpolation grid
    if showGrids:
        title = 'original limit with interp. grid and contours (cubic-geom. interp.)'
    else:
        title = 'original limit with contours (cubic-geom. interp.)'
    draw_interp_subplot(ax13, h, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_geom, title = title, showGrids = showGrids, cont = cont)
    print 'Cubic interpolation (geom) done'

    ## fourth subplot: original limit with contours from cubic-min_E interpolation and interpolation grid
    if showGrids:
        title = 'original limit with interp. grid and contours (cubic-minE interp.)'
    else:
        title = 'original limit with contours (cubic-min_E interp.)'
    draw_interp_subplot(ax14, h, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_min_E, title = title, showGrids = showGrids, cont = cont)
    print 'Cubic interpolation (minE) done'


    # second figure
    fig2, ((ax21, ax22), (ax23, ax24)) = plt.subplots(nrows=2, ncols=2, figsize=(25, 25))
    cont = [0.01,0.02,0.03]

    # first subplot: triangulation of limit plot and interp. limit plot
    if showGrids:
        title = 'interp. limit with triangular grid'
        draw_interp_subplot(ax21, h, x_spec, y_spec, z_spec, triang = triang, title = title, showGrids = showGrids, cont = cont)
    else:
        title = 'interp. limitplot'
        draw_interp_subplot(ax21, h, x_spec, y_spec, z_spec, title = title, showGrids = showGrids, cont = cont)
    print 'Triangular grid done'

    # second subplot: interp. limit with contours from linear interpolation and interpolation grid
    if showGrids:
        title = 'interp. limit with interp. grid and contours (linear interp.)'
    else:
        title = 'interp. limit with contours (linear interp.)'
    draw_interp_subplot(ax22, zi_lin, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_lin, title = title, showGrids = showGrids, cont = cont, triang = triang)
    print 'Linear interpolation done'

    ## third subplot: interp. limit with contours from cubic-geom. interpolation and interpolation grid
    if showGrids:
        title = 'interp. limit with interp. grid and contours (cubic-geom. interp.)'
    else:
        title = 'interp. limit with contours (cubic-geom. interp.)'
    draw_interp_subplot(ax23, zi_cubic_geom, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_geom, title = title, showGrids = showGrids, cont = cont, triang = triang)
    print 'Cubic interpolation (geom) done'

    ## fourth subplot: interp. limit with contours from cubic-min_E interpolation and interpolation grid
    if showGrids:
        title = 'interp. limit with interp. grid and contours (cubic-minE interp.)'
    else:
        title = 'interp. limit with contours (cubic-min_E interp.)'
    draw_interp_subplot(ax24, zi_cubic_min_E, x_spec, y_spec, z_spec, xi = xi, yi = yi, zi = zi_cubic_min_E, title = title, showGrids = showGrids, cont = cont, triang = triang)
    print 'Cubic interpolation (minE) done'

    plt.show()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('inputFile',
                        help='Text file with lp limit values.')
    parser.add_argument("--checkInterp",
                        action="store_true",
                        default=False,
                        help="Produce extra plots to show the interpolation.")
    parser.add_argument("--show",
                        action="store_true",
                        default=False,
                        help="Show plots.")
    parser.add_argument("--showGrids",
                        action="store_true",
                        default=False,
                        help="Show grids used for interpolation")
    parser.add_argument("--mHFile",
			type = str,
                        default='higgs_masses.txt',
                        help="File with higgs masses")
    parser.add_argument('-t', '--tag',
			type=str,
                        default=None,
                        help='Tag added to plot names')
    args = parser.parse_args()
    log.basicConfig(level=log.WARNING)
    # observed limit
    draw_lp_limit_from_table(7, args)
    # expected limit
    draw_lp_limit_from_table(4, args)


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()

